
# CYSAVE

CYber Security for Autonomous VEssel (CYSAVE) is a union of datasets and codes
used to infer autonomous system state in order to improve overall cyber security.

Every dataset contains a labeled greyscale images for every system state generated starting from logs informations.
We have seven different datsets generated by the first one simply removing some logged variables.
The first dataset no\_none contains all logged variables.
The last datasets no\_cellsensors\_encrypted and no\_cellsensors\_plaintext have the minimum number of variables.

The codes contains a simple Convolutional Neural Network implementation,
and a simple Multi Layer Perceptron implementation written in Keras.
This Deep Neural Networks are trained on these dataset in order to study their classification
abilities on this type of classes.

## Brief description

## How to use the tool

### Prerequisites

The codes are entirely written in Python 3.6 and Keras on Tensorflow on Ubuntu 16.04 and tested on Linux,
Windows and Mac OS. The following tool/libraries must be installed to work
properly:

* [Python](https://www.python.org/downloads/)
* [Tensorflow in Anaconda](https://www.tensorflow.org/install/install_linux#InstallingAnaconda)
* [SciPy](https://scipy.org/install.html)

### Cloning and first run

#### Cloning

The project can be easily cloned via `git`
(`git clone https://gitlab.com/mmolivato/CYSAVE.git`) or it can be downloaded from
the [project main page](https://gitlab.com/mmolivato/CYSAVE).

#### First run

    user@host:~$ ~/CYSAVE$ source activate tensorflow
    (tensorflow) user@host:~$ ~/CYSAVE$ cd Codes
    (tensorflow) user@host:~$ ~/CYSAVE/Codes$ python main.py

