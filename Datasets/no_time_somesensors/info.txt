

####### DATASET TRAINING INFORMATIONS
Dataset x shape : (3554, 31, 31)
Dataset y shape : (3554,)
Datasets num of different classes: 6
Dictionary of label name for each class: {0: 'dos_0', 1: 'dos_32', 2: 'fault_gpsdown', 3: 'fault_stucked', 4: 'good', 5: 'stolen'}

####### DATASET TEST INFORMATIONS
Dataset x shape : (14228, 31, 31)
Dataset y shape : (14228,)
Datasets num of different classes: 6
Dictionary of label name for each class: {0: 'dos_0', 1: 'dos_32', 2: 'fault_gpsdown', 3: 'fault_stucked', 4: 'good', 5: 'stolen'}