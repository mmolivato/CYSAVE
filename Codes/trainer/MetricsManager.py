'''
Created on 02 mar 2018

@author: olivato
'''
import numpy as np

from operator import add, truediv, mul

class MetricsManager(object):
    '''
    classdocs
    '''

    def __init__(self, model_name, dataset_name, score, confusion_matrix, history, number_of_decimals = 3):
        '''
        Constructor
        '''
        self.model_name = model_name
        self.dataset_name = dataset_name
        self.confusion_matrix = confusion_matrix
        self.history = history
        self.number_of_decimals = number_of_decimals
        
        # dictionary of calculated metrics
        self.metrics = {}
        self.metrics['overall_loss'] = self.__formatFloatVal(score[0])
        self.metrics['overall_accuracy'] = self.__formatFloatVal(score[1])
        # calc all standard metrics
        self.__calcMetrics()
    
    def __formatFloatVal(self, flt, val=100):
        formf = '%.' + str(self.number_of_decimals) + 'f'
        return float(formf%(flt*val))
    
    def __formatFloatsList(self, l, val=100):
        # returns the list of float formatted with desired decimals
        return [self.__formatFloatVal(x, val) for x in l]
    
    def __getListPercentage(self, l, tot):
        # returns the percentage value of a list refferred to a specific total value for each element
        return self.__formatFloatsList([l[i] * 100.0 / tot[i] for i in range(len(l))], 1)
    
    def __calcTPFPFNTN(self):
        # gets the number of classes
        self.n_classes = len(self.confusion_matrix)
        # founds true positve values for each class
        self.TruePositive = np.diag(self.confusion_matrix)
        # founds false positve values for each class
        self.FalsePositive = [sum(self.confusion_matrix[:,i]) - self.confusion_matrix[i,i] for i in range(self.n_classes)]
        # founds false negative values for each class
        self.FalseNegative = [sum(self.confusion_matrix[i,:]) - self.confusion_matrix[i,i] for i in range(self.n_classes)]
        # founds true negative values for each class
        self.TrueNegative = [sum(sum(np.delete(np.delete(self.confusion_matrix, i, 0), i, 1))) for i in range(self.n_classes)]
    
    def __calcSupportLists(self):
        # total positive values
        self.TotPositive = list(map(add, self.TruePositive, self.FalsePositive))
        # total negative values
        self.TotNegative = list(map(add, self.TrueNegative, self.FalseNegative))
        # total true values
        self.TotTrue = list(map(add, self.TruePositive, self.TrueNegative))
        
        # sum of true postivie and false negative
        self.TPFN = list(map(add, self.TruePositive, self.FalseNegative))
        # sum of true negative and false positive
        self.TNFP = list(map(add, self.TotNegative, self.FalsePositive))
        
        # total of all values
        self.Total =  (map(add, self.TotPositive, self.TotNegative))
    
    def __calcAdvancedMetrics(self):
        # calcs the accuracy for each class
        self.metrics['accuracy'] = list(map(truediv, self.TotTrue, self.Total))
        # calcs the precision for each class
        self.metrics['precision'] = list(map(truediv, self.TruePositive, self.TotPositive))
        # calcs the recall for each class
        self.metrics['recall'] = list(map(truediv, self.TruePositive, self.TPFN))
        # calcs NPV for each class
        self.metrics['npv'] = list(map(truediv, self.TrueNegative, self.TotNegative))
        # calcs the specificity for each class
        self.metrics['specificity'] = list(map(truediv, self.TrueNegative, self.TNFP))
        # calcs the fall-out for each class
        self.metrics['fall-out'] = list(map(truediv, self.FalsePositive, self.TNFP))
        
        # calcs the F1 score
        self.metrics['F1'] = self.getFBetaScore(1, self.metrics['precision'], self.metrics['recall'])
        # calcs the F2 score
        self.metrics['F2'] = self.getFBetaScore(2, self.metrics['precision'], self.metrics['recall'])
        
        # foreach metrics mul 100.0 and round result
        for k in self.metrics:
            if type(self.metrics[k]) == list:
                self.metrics[k] = self.__formatFloatsList(self.metrics[k])
        
    def __calcMetrics(self):
        # first calc true_positive, false_positive, true_negative, false_negative
        self.__calcTPFPFNTN()
        # second calc support list used for advanced metrics
        self.__calcSupportLists()
        # third cal advanced metrics
        self.__calcAdvancedMetrics()
    
    def getTPFPFNTN(self):
        return self.TruePositive, self.FalsePositive, self.TrueNegative, self.FalseNegative
    
    def getTPFPFNTN_Percentage(self):
        res = (self.__getListPercentage(self.TruePositive, self.TotPositive),
               self.__getListPercentage(self.FalsePositive, self.TotPositive),
               self.__getListPercentage(self.TrueNegative, self.TotNegative),
               self.__getListPercentage(self.FalseNegative, self.TotNegative))
        return res
    
    def getFormattedMetrics(self):
        fmetrics = {}
        
        for i in self.metrics:
            fmetrics[i] = self.__formatFloatsList(self.metrics[i])
            
    def getFBetaScore(self, beta, precision, recall):
        # returns the F beta score value: (1+B^2)*precision*recall / B^2*precision*recall
        pmr = [(1 + beta ** 2) * x for x in list(map(mul, precision, recall))]
        par = list(map(add, [beta**2 * x for x in precision], recall))
        return list(map(truediv, pmr, par))
    
    def checkValidity(self, test_len, num_classes):
        # check the confusion matrix validity
        if not all([ self.TruePositive[i] + self.FalsePositive[i] + self.TrueNegative[i] + self.FalseNegative[i] == test_len for i in range(num_classes) ]):
                    raise ValueError('Confusion matrix and sum of TP FP TN FN on total num of tests doesn\'t match.')
    
    def updateMetrics(self):
        # update all metrics
        self.__calcMetrics()
    