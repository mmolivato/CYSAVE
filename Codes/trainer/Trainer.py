'''
Created on 24 feb 2018

@author: olivato
'''
import numpy as np
from sklearn.metrics import confusion_matrix

import keras
from keras import backend as K
from outinfo_manager.OutInfoManager import OutInfoManager
from trainer.MetricsManager import MetricsManager

class Trainer(object):
    '''
    classdocs
    '''

    def __init__(self, dnn_models_dict, datasets_dirs_dict , batch_size, epochs, num_classes, outputpath, labels_names_dict):
        '''
        Constructor
        '''
        self.dnn_models_dict = dnn_models_dict
        self.datasets_dirs_dict = datasets_dirs_dict
        self.batch_size = batch_size
        self.epochs = epochs
        self.num_classes = num_classes
        self.outputpath = outputpath
        self.labels_names_dict = labels_names_dict
        
        # generate a out info manager
        self.out_info_manager = OutInfoManager(outputpath, epochs, labels_names_dict)
        
    def loadDatasets(self, dsets_dir):
        dtrain = np.load(dsets_dir + 'training_dset.npz')
        dtest = np.load(dsets_dir + 'test_dset.npz')
        
        # save test datasets for validation phase
        self.y_test_pred = dtest['y']
        
        return dtrain['x'], dtrain['y'], dtest['x'], dtest['y']
    
    def setInputShape(self, x_train, x_test):
        # input image dimensions
        (numx, img_rows, img_cols) = x_train.shape
        
        if K.image_data_format() == 'channels_first':
            x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
            x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
            input_shape = (1, img_rows, img_cols)
        else:
            x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
            x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
            input_shape = (img_rows, img_cols, 1)
        
        self.input_shape = input_shape
        return x_train, x_test
    
    def setupDatasets(self, x_train, y_train, x_test, y_test):
        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
                
        # convert class vectors to binary class matrices
        y_train = keras.utils.to_categorical(y_train, self.num_classes)
        y_test = keras.utils.to_categorical(y_test, self.num_classes)
        
        return x_train, y_train, x_test, y_test

    def backupWeights(self, model):
        self.old_weights = model.get_weights()
    
    def resetModelWeights(self, model):
        model.set_weights(self.old_weights)
    
    def trainAndGenerateResults(self):
        
        metrics_managers = []
        
        # for every datasets type
        for ds in self.datasets_dirs_dict:
            # for every model to train
            for m in self.dnn_models_dict:
                
                # get datasets ready to use
                x_train, y_train, x_test, y_test = self.loadDatasets(self.datasets_dirs_dict[ds])
                x_train, y_train, x_test, y_test = self.setupDatasets(x_train, y_train, x_test, y_test)
                    
                # get correct input shape
                x_train, x_test = self.setInputShape(x_train, x_test)
                
                # get model to train
                model = self.dnn_models_dict[m](self.input_shape, self.num_classes)
                
                # save graph model
                self.out_info_manager.saveGraphModel(model, m)
                
                # compile the model
                model.compile(loss = keras.losses.categorical_crossentropy,
                              optimizer = keras.optimizers.Adadelta(),
                              metrics = ['accuracy'])
                
                # backup model weights
                self.backupWeights(model)
                # printing the model summary (every neural network layer and output)
                model.summary()
                
                # fit the model
                history = model.fit(x_train, y_train,
                                    batch_size = self.batch_size,
                                    epochs = self.epochs,
                                    verbose = 1,
                                    validation_data = (x_test, y_test))
                
                # evaluates the model
                score = model.evaluate(x_test, y_test, verbose = 1)
                # get the confusion matrix
                conf_mat = confusion_matrix( self.y_test_pred, model.predict_classes(x_test))
                
                # save metrics manager for this test
                mm = MetricsManager(m, ds, score, conf_mat, history)
                mm.checkValidity(len(y_test), self.num_classes)
                metrics_managers.append(mm)
                
        self.out_info_manager.saveAllInfos(metrics_managers)
                