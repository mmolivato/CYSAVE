'''
Created on 25 feb 2018

@author: olivato
'''


from trainer.Trainer import Trainer

import os
from dnn_models.simple_cnn import SimpleCNN
from dnn_models.simple_mlp import SimpleMLP

def getDirsNames(path):
    cwd = os.getcwd()
    os.chdir(path)
    ret = [name for name in os.listdir(".") if os.path.isdir(name)]
    os.chdir(cwd)
    return ret

if __name__ == '__main__':
    # dictionary containing name of a dnn model and its build funciton
    dnn_models_dict = {'CNN': SimpleCNN.getModel,
                       'MLP': SimpleMLP.getModel}
    
    # size of the batch of the input data in a single train step
    batch_size = 32
    # number of epochs
    epochs = 1
    # number of classes to classify
    num_classes = 6
    
    # datasets path and relative dictionary
    dsets_path = '../../Datasets/'
    dirs_names = getDirsNames(dsets_path)
    datasets_dirs_dict = dict(zip(dirs_names, [dsets_path + dn + '/' for dn in dirs_names]))
    
    # output path
    outputpath = '../../dnn_results/'
    # labels names
    labels_names_dict = {0: 'dos_0', 1: 'dos_32', 2: 'fault_gpsdown', 3: 'fault_stucked', 4: 'good', 5: 'stolen'}
    
    # create trainer
    trainer = Trainer(dnn_models_dict, datasets_dirs_dict , batch_size, epochs, num_classes, outputpath, labels_names_dict)
    
    # train all datasets on all models
    trainer.trainAndGenerateResults()
