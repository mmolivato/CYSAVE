'''
Created on 24 feb 2018

@author: olivato
'''

class SimpleMLP():
    
    @staticmethod
    def getModel(input_shape, num_classes):
        import keras
        from keras.models import Sequential
        from keras.layers import Dense, Dropout, Flatten
        
        model = Sequential()
        
        model.add(Flatten(input_shape = input_shape))
        model.add(Dense(512, activation='relu'))
        model.add(Dropout(0.25))
        model.add(Dense(256, activation='relu'))
        model.add(Dropout(0.25))
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.25))
        model.add(Dense(num_classes, activation='softmax'))
        
        model.compile(loss = keras.losses.categorical_crossentropy,
                  optimizer = keras.optimizers.Adadelta(),
                  metrics = ['accuracy'])
                    
        return model