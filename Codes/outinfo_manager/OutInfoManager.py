'''
Created on 02 mar 2018

@author: olivato
'''
import numpy as np
import os
from keras.utils import plot_model
import matplotlib.pyplot as plt

class OutInfoManager(object):
    '''
    classdocs
    '''
    
    def __init__(self, outpath, epochs, labels_names_dict, plots_path='plots/', text_info_path='', tex_tabels_path ='tex_tables/',  img_extension = '.png'):
        '''
        Constructor
        '''
        self.outpath = outpath
        self.epochs = epochs
        self.labels_names_dict = labels_names_dict
        self.plots_path = self.outpath + plots_path
        self.text_info_path = self.outpath + text_info_path
        self.tex_tabels_path = self.outpath + tex_tabels_path
        self.img_extension = '.png'
        # makes all dirs
        self.__makePaths()
    
    def __makePaths(self):
        
        paths = [self.outpath, self.plots_path, self.text_info_path, self.tex_tabels_path]
        
        # creates folders if they do not exist
        for p in paths:
            if not os.path.exists(p):
                os.mkdir(p)
    
    def saveGraphModel(self, model, model_name):
        # plot graph of the model
        pathimg = self.plots_path + model_name + '.png'
        
        if not os.path.exists(pathimg):
            plot_model(model, to_file = pathimg)
    
    def savePlot(self, title, xlabel, ylabel, legend, vals, namefile, show = False):
        
        for vls in vals:
            plt.plot(vls)
            
        plt.title(title)
        plt.ylabel(ylabel)
        plt.xlabel(xlabel)
        plt.legend(legend, loc='upper left')
        if show:
            plt.show()
            
        plt.savefig(self.plots_path + namefile + self.img_extension)
        plt.close()
    
    def saveAccuracyAndLossPlots(self, metrics_manager):
        # if the model is trained more than one epoch
        if(self.epochs > 1):
            # summarize history for accuracy
            title = 'Accuracy - Model: ' + metrics_manager.model_name + ' Dataset: ' + metrics_manager.dset_name
            xlabel = 'epoch'
            ylabel = 'accuracy'
            legend = ['train', 'test']
            vals = [metrics_manager.history.history['acc'], metrics_manager.history.history['val_acc']]
            namefile = ylabel + '_' + metrics_manager.model_name + '_' + metrics_manager.dset_name
            
            self.savePlot(title, xlabel, ylabel, legend, vals, namefile)
            
            # summarize history for loss
            title = 'Loss - Model: ' + metrics_manager.model_name + ' Dataset: ' + metrics_manager.dset_name
            ylabel = 'loss'
            vals = [metrics_manager.history.history['loss'], metrics_manager.history.history['val_loss']]
            namefile = ylabel + '_' + metrics_manager.model_name + '_' + metrics_manager.dset_name
            
            self.savePlot(title, xlabel, ylabel, legend, vals, namefile)

    def saveConfusionMatrixPlot(self, metrics_manager):
        # normalized confusion matrix
        norm_conf_mat = np.array([[float(j) / float(sum(i)) for j in i] for i in metrics_manager.confusion_matrix])
        
        # create the figure
        fig = plt.figure()
        plt.clf()
        ax = fig.add_subplot(111)
        ax.set_aspect(1)
        
        # creates the confusion matrix
        res = ax.imshow(norm_conf_mat, cmap = plt.get_cmap('coolwarm'), interpolation = 'nearest')
        
        # inserts annotation with the number of istances in boxes
        width, height = metrics_manager.confusion_matrix.shape
        for x in range(width):
            for y in range(height):
                ax.annotate(str(metrics_manager.confusion_matrix[x][y]), xy = (y, x),
                            horizontalalignment ='center',
                            verticalalignment = 'center')
        
        # add colorbar
        fig.colorbar(res)
        
        # add labels to the axes
        plt.xticks(range(width), self.labels_names_dict.values(), rotation = 90)
        plt.yticks(range(height), self.labels_names_dict.values())
        
        fignamepath = self.plots_path  + 'confmat_' + metrics_manager.model_name + '_' + metrics_manager.dataset_name + '.png'
        # save the figure
        plt.savefig(fignamepath, format = 'png', bbox_inches = "tight")
        
    def saveFileResultsToText(self, metrics_manager):
        # path of the file
        path = self.text_info_path + metrics_manager.model_name + '_train_results.txt'
        
        lines = []
        lines.append('\n\n####### MODEL : ' + metrics_manager.model_name + ' AND DATASET : ' + metrics_manager.dataset_name + ' RESULTS\n')
        # TODO: print model summary
        # lines.append('\nModel Summary : \n' + str(model.summary()))
        lines.append('\nTest loss % : ' + str(metrics_manager.metrics['overall_loss']))
        lines.append('\nTest accuracy % : ' + str(metrics_manager.metrics['overall_accuracy']))
        lines.append('\nConfusion Matrix : \n' + str(metrics_manager.confusion_matrix))
        lines.append('\nTrue Positive : ' + str(metrics_manager.TruePositive))
        lines.append('\nFalse Positive : ' + str(metrics_manager.FalseNegative))
        lines.append('\nTrue Negative : ' + str(metrics_manager.TrueNegative))
        lines.append('\nFalse Negative : ' + str(metrics_manager.TruePositive))
        lines.append('\n')
        
        (TPperc, FPperc, FNperc, TNperc) = metrics_manager.getTPFPFNTN_Percentage()
        lines.append('\nTrue Positive % : ' + str(TPperc))
        lines.append('\nFalse Positive % : ' + str(FPperc))
        lines.append('\nTrue Negative % : ' + str(FNperc))
        lines.append('\nFalse Negative % : ' + str(TNperc))
        
        # print advanced metrics
        lines.append('\nAccuracy for each class % : ' + str(metrics_manager.metrics['accuracy']))
        lines.append('\nPrecision for each class % : ' + str(metrics_manager.metrics['precision']))
        lines.append('\nRecall for each class % : ' + str(metrics_manager.metrics['recall']))
        lines.append('\nNPV for each class % :' + str(metrics_manager.metrics['npv']))
        lines.append('\nSpecificity for each class % :' + str(metrics_manager.metrics['specificity']))
        lines.append('\nFall-out for each class % :' + str(metrics_manager.metrics['fall-out']))
        lines.append('\nF1 score for each class % :' + str(metrics_manager.metrics['F1']))
        lines.append('\nF2 score for each class % :' + str(metrics_manager.metrics['F2']))
        
        # save dsets info in a textfile
        with open(path, 'a') as f:
            f.writelines(lines)

    def convMatToTexTable(self, corner_header,  xheader, yheader, table, ntabulation = 2):
        textab = []
        
        textab.append('\\begin{adjustbox}{max width=\\textwidth}')
        # finds the num of columns
        textab.append('\t\\begin{tabular}{|' + 'l|' * (len(table[0]) + 1) + '} \\hline')
        
        # tabulation for the table
        pad = '\t' * ntabulation
        
        # add header
        header = [pad + corner_header] + xheader
        textab.append((pad + ' & ').join(header))
        
        # add all rows
        for i in range(len(yheader)):
            row = (pad +' & ').join([pad + yheader[i]] + [str(n) for n in table[i]]) + ' \\\\ \\hline'
            textab.append(row)
        
        # end of the tabular
        textab.append('\t\\end{tabular}')
        textab.append('\\end{adjustbox}')
        
        return '\n'.join(textab)
        
    def boldMaxValue(self, table, bmax_rows, bmax_cols, search_max = True):
        import copy
        
        tab = copy.deepcopy(table)
        
        # find the max position in the subtable
        for i in range(0, len(tab), bmax_rows):
            for j in range(0, len(tab[0]), bmax_cols):
                
                # starts to find max in the subtable
                desired_k = -1
                desired_l = -1
                for k in range(i, min(i + bmax_rows, len(tab))):
                    for l in range(j, min(j + bmax_cols, len(tab[0]))):
                        # the first value in the subtable
                        if desired_k + desired_l < 0 and desired_k + desired_l > -3:
                            desired_k, desired_l = k, l
                        # if i've a max
                        elif desired_k + desired_l > -3:
                            if search_max:
                                # if there's a new max
                                if tab[desired_k][desired_l] < tab[k][l]:
                                    desired_k, desired_l = k, l
                                # if are equals cancel everything
                                elif tab[desired_k][desired_l] == tab[k][l]:
                                    desired_k, desired_l = -2, -2
                                    break
                            else:
                                # if there's a new min
                                if tab[desired_k][desired_l] > tab[k][l]:
                                    desired_k, desired_l = k, l
                                # if are equals cancel everything
                                elif tab[desired_k][desired_l] == tab[k][l]:
                                    desired_k, desired_l = -2, -2
                                    break
                                
                    if desired_k + desired_l < -3:
                        break
                
                if desired_k + desired_l > -3 :
                    # i've foud a max in subtable
                    tab[desired_k][desired_l] = '\\textbf{ ' + str(tab[desired_k][desired_l]) + ' }'
        return tab
    
    def saveAllInfos(self, metrics_managers):        
        # save all informations: text, tex, images
        
        for m in metrics_managers:
            # save accuracy and loss plots
            self.saveAccuracyAndLossPlots(m)
            # save confusion matrix plot
            self.saveConfusionMatrixPlot(m)
            # save file resuts to text
            self.saveFileResultsToText(m)